package Helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
	
	public static WebDriver driver;

	public static WebDriver start_Browser(String browserName, String url) {
		
		if (browserName.equalsIgnoreCase("Chrome") )
		{
			String exePath = "/home/naresh/chromedriver";
			System.setProperty("webdriver.chrome.driver", exePath);
			driver = new ChromeDriver();
	    }
		else
		{
			String exePath = "/home/naresh/Downloads/geckodriver"; 
			System.setProperty("webdriver.gecko.driver", exePath);
			driver = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		
		return driver;		
	}
	
	
	
	
	

}
