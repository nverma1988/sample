package com.commonmethods;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.archivesmart.pages.LoginPage;
import com.archivesmart.pages.TreePage;

public class CommonMethods {
	

		public static void login(WebDriver driver ,String uname,String pwd)
		
		{
			driver.findElement(LoginPage.email).sendKeys(uname);
			driver.findElement(LoginPage.next).click();
			driver.findElement(LoginPage.password).sendKeys(pwd);
			driver.findElement(LoginPage.email).sendKeys(Keys.RETURN);
			/*Actions make  = new Actions(driver);
			 Action kbEvents = make.keyDown(Keys.RETURN).build();
			 kbEvents.perform();*/

			
			
		}
		
        public static void logout(WebDriver driver, By uname) throws InterruptedException
		
		{
	        hoverAndClick(driver,uname);
			driver.findElement(TreePage.Exit).click();
			

			
			
		}
        public static void hoverAndClick(WebDriver driver,By elementToHover) throws InterruptedException 
        
        
        {
         Thread.sleep(2000);
	     Actions action = new Actions(driver);
	     action.moveToElement(driver.findElement(elementToHover)).build().perform();
         }
        

}
